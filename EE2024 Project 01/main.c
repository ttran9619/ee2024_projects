#include "stdio.h"
#define N_MAX 10
#define X_SIZE_MAX 11


// EE2024 Assignment 1, Sem 2, AY 2015/16
// Rajesh Panicker, ECE NUS, 2015

extern int fir(int N, int* b, int x, int reset); // asm implementation w/ Circular Buffer
extern int firSHIFT(int N, int* b, int x, int reset); // asm implementation w/ Reference Shift Buffer
int fir_c(int N, int* b, int x, int reset); // reference C implementation



int main(void)
{
	//variables
	int j;
	int N = 10;
	int x_size = 11;
	// think of the values below as numbers of the form y.yy (floating point with 2 digits precision)
	// which are scaled up to allow them to be used integers
	// within the fir function, we divide y by 10,000 (decimal) to scale it down
	int b[N_MAX+1] = {100, 300, 200, 400}; //N+1 dimensional
	int x_sig[X_SIZE_MAX] = {100, 200, 300, 400, 500, 600, 500, 400, 300, 200, 100};


	fir(0, 0, 0, 1); // reset the filter. The first three arguments are not used
	fir_c(0, 0, 0, 1); // reset the filter
	firSHIFT(0, 0, 0, 1);

	// Call assembly language function fir for each element of x_sig
	for (j=0; j<x_size; j++) // first run
	{
		printf( "Run 1 asm: j = %d, y = %d, \n", j, fir(N, b, x_sig[j], 0) ) ; // last parameter should be zero unless we wish to reset the filter
		printf( "Run 1 C  : j = %d, y = %d, \n", j, fir_c(N, b, x_sig[j], 0) ) ;
		printf( "Run 1 asmSHIFT: j = %d, y = %d, \n", j, firSHIFT(N, b, x_sig[j], 0) ) ;
	}
	for (j=0; j<x_size; j++) // second run
	{
		printf( "Run 2 asm: j = %d, y = %d, \n", j, fir(N, b, x_sig[j], 0) ) ;
		printf( "Run 2 C  : j = %d, y = %d, \n", j, fir_c(N, b, x_sig[j], 0) ) ;
		printf( "Run 2 asmSHIFT: j = %d, y = %d, \n", j, firSHIFT(N, b, x_sig[j], 0) ) ;
	}

	fir(0, 0, 0, 1); // reset the filter
	fir_c(0, 0, 0, 1); // reset the filter
	firSHIFT(0, 0, 0, 1);

	for (j=0; j<x_size; j++) // third run, following reset
	{
		printf( "Run 3 asm: j = %d, y = %d, \n", j, fir(N, b, x_sig[j], 0) ) ;
		printf( "Run 3 C  : j = %d, y = %d, \n", j, fir_c(N, b, x_sig[j], 0) ) ;
		printf( "Run 3 asmSHIFT: j = %d, y = %d, \n", j, firSHIFT(N, b, x_sig[j], 0) ) ;
	}
	while (1) { //halt
	}
}

int fir_c(int N, int* b, int x, int reset)
{ // the implementation below is inefficient and meant only for verifing your results
	static int filter[N_MAX] = {0};
	int i;
	int y;
	int x_store;
	x_store = x;
	if(reset != 0) // if reset != 0 , reset the filter
	{
		for(i=0; i<N_MAX; i++)
		{
			filter[i] = 0;
		}
		x = 0;
	}
	else
	{
		x*=b[0];
		for(i=0; i<N; i++)
		{
			x+=b[i+1]*filter[i]; // ok since a is N+1 dimensional
		}
		for(i=N-1; i>0; i--)   // x(n-i)'s move forward by 1. Note : inefficient implementation
		{
			filter[i] = filter[i-1];
		}
		filter[0] = x_store;
	}
	y = x/10000; // scaling down
	return y;
}
