 	.syntax unified
 	.cpu cortex-m3
 	.thumb
 	.align 2
 	.global	firSHIFT
 	.thumb_func

@ EE2024 Assignment 1, Sem 2, AY 2015/16
@ (c) Rajesh Panicker and CK Tham, ECE NUS, 2016

@Register map
@R0 - N, returns y
@R1 - b
@R2 - x
@R3 - reset
@R4 - <use(s)>
@R5 - <use(s)>
@....

firSHIFT:
@ save only those registers which are modified by your function
@ parameter registers need not be saved.
	PUSH {R4, R5, R6, R7, LR}
@ write asm code here

@ prepare parameters to pass to subroutine (if used)

@ call subroutines (if used)
@		BL SUBROUTINE

@ write asm code here
	CMP R3, #0		@Branch to reset subroutine
	IT NE
	BLNE reset

	CMP R3, #0		@Branch to fill subroutine
	IT EQ
	BLEQ fill

@ prepare value to return to C program in R0
	MOV R0, R2
	LDR R2, =#10000	@Storing 10000 in a register to use in SDIV
	SDIV R0, R2		@Scaling the return value

@ recover original register values. DO NOT save or restore R0. Why?
	POP {R4, R5, R6, R7, LR}
@ return to C program
		BX	LR

@ start of subroutine (if used)
@SUBROUTINE:
@ save registers that will be modified by your subroutine

@ write asm code for body of subroutine here

@ prepare value to return to calling program

@ recover original register values

@ return to calling program
@		BX LR

reset:
	LDR R0, =#N_MAX		@Getting N_MAX for counting
	LDR R1, =FILTER		@Storing FILTER Address at R1
	MOV R2, #0			@Getting a 0 val in a register & clearing x val

	loopR:
		STR R2,[R1],#4		@Clearing FILTER addr and incrementing
		SUBS R0, #1			@Decrementing N_MAX, using as a counter
		BNE loopR			@Loops until N_MAX = 0

	BX LR
fill:
	MOV R3, R2	@Creating x_store

	LDR R4, [R1]	@Performing x*=b[0];
	MUL R2, R4

	MOV R4, #1		@Creating counter
	MOV R7, R4

	LDR R6, =FILTER	@Storing FILTER Address at R6
	loopA:
		LDR R4, [R1,#4]! 		@Loading b[i]
		LDR R5, [R6], #4		@Loading FILTER[n-i]

		MLA R2, R4, R5, R2		@x=x+b[i]*FILTER[n-i]

		ADD R7, #1				@Incrementing until i=N
		CMP R7, R0
		IT NE
		BNE loopA
	loopB:
		LDR R4, [R6, #-4]		@Performing filter[i] = filter[i-1];
		STR R4, [R6], #-4

		SUB R7, #1				@@Decrementing until i=0
		CMP R7, #1
		IT NE
		BNE loopB

	LDR R6, =FILTER	@Storing FILTER Address at R6
	STR R3, [R6]
	BX LR

@label: .word value
	.equ N_MAX, 10
@.lcomm label num_bytes
	.lcomm	FILTER	N_MAX*4
