 	.syntax unified
 	.cpu cortex-m3
 	.thumb
 	.align 2
 	.global	fir
 	.thumb_func

@ EE2024 Assignment 1, Sem 2, AY 2015/16
@ (c) Rajesh Panicker and CK Tham, ECE NUS, 2016

@Register map
@R0 - N, returns y
@R1 - b, <Immediate 10000>
@R2 - x, x sum, scaled x, <Immediate 0>
@R3 - reset	<b[i] value in fill and last BUFFER index in fill>
@R4 - <BUFFER[n-i] value, POINTER ref.>
@R5 - <POINTER addr.>
@R6 - <BUFFER addr.>
@R7 - <N_MAX_WORD value, and N_MAX in reset>
@....

fir:

@ save only those registers which are modified by your function
@ parameter registers need not be saved.
	PUSH {R4, R5, R6, R7, LR}
@ write asm code here

	LDR R6, =BUFFER		@Getting BUFFER addr.
	LDR R4, =POINTER	@Loading the the POINTER reference
	LDR R5, [R4]		@Loading the the POINTER addr.

	CMP R5, #0			@Initalizing static POINTER addr. to BUFFER addr.
	IT EQ
	STREQ R6, [R4]

@ prepare parameters to pass to subroutine (if used)

@ call subroutines (if used)
@		BL SUBROUTINE

@ write asm code here
	CMP R3, #0		@Branch to reset subroutine
	IT NE
	BLNE reset

	CMP R3, #0		@Branch to fill subroutine
	IT EQ
	BLEQ fill


@ prepare value to return to C program in R0
	MOV R0, R2		@Moving x sum into R0
	LDR R1, =#10000	@Storing 10000 in a register to use in SDIV
	SDIV R0, R1		@Scaling the return value

@ recover original register values. DO NOT save or restore R0. Why?
	POP {R4, R5, R6, R7, LR}
@ return to C program
	BX	LR

@ start of subroutine (if used)
@SUBROUTINE:

@ save registers that will be modified by your subroutine

@ write asm code for body of subroutine here

@ prepare value to return to calling program

@ recover original register values

@ return to calling program
@		BX LR
reset:
	LDR R7, =#N_MAX		@Getting N_MAX for counting
	MOV R1, #0			@Getting a 0 val in a register & clearing x val

	loopReset:
		STR R1,[R6],#4		@Clearing BUFFER addr and incrementing
		SUBS R7, #1			@Decrementing N_MAX, using as a counter
		BNE loopReset		@Loops until N_MAX = 0

	BX LR
fill:
	STR R2, [R5]		@Storing the new x value in the BUFFER[n]
	MOV R2, #0			@Clearing the x value

	LDR R7, =#N_MAX_WORD	@Getting N_MAX_WORD for wrapping

	loopFIR:
		LDR R3, [R1], #4 		@Loading b[i]
		LDR R4, [R5], #-4		@Loading BUFFER[n-i]

		MLA R2, R3, R4, R2		@x=x+b[i]*BUFFER[n-i]

		CMP R5, R6				@Checking for POINTER addr. < BUFFER[0]
		IT LO
		ADDLO R5, R7 			@Wrapping the POINTER

		SUBS R0, #1				@Decrementing counter
		BNE loopFIR

	LDR R4, =POINTER		@Incrementing the POINTER addr.
	LDR R5, [R4]
	ADD R5, #4
	STR R5, [R4]

	SUB R7, #4				@Getting the BUFFER[N_MAX-1]
	ADD R3, R6, R7

	CMP R5, R3				@Checking for POINTER > BUFFER[N_MAX-1]
	IT HI
	STRHI R6, [R4]			@Resetting POINTER addr.

	BX LR

@label: .word value
	.equ N_MAX, 10
	.equ N_MAX_WORD, N_MAX*4	@Separate .equ to avoid needed to use a MUL instruction in code
@.lcomm label num_bytes
	.lcomm	POINTER 4
	.lcomm	BUFFER	N_MAX_WORD

	.end
