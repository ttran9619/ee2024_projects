/*****************************************************************************
 *   A demo example using several of the peripherals on the base board
 *
 *   Copyright(C) 2011, EE2024
 *   All rights reserved.
 *
 ******************************************************************************/

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_timer.h"
#include "lpc17xx_uart.h"

#include "joystick.h"
#include "pca9532.h"
#include "acc.h"
#include "oled.h"
#include "rgb.h"
#include "light.h"
#include "string.h"
#include "temp.h"
#include "led7seg.h"
#include "main.h"
//#include "uart2.h" //Not Needed
#include <stdio.h>

//Static Variables
//Joystick
static volatile int8_t joystick_down = 0;
static volatile int8_t joystick_up = 0;
static volatile int8_t joystick_left = 0;
static volatile int8_t joystick_right = 0;
static volatile int8_t joystick_pressed = 0;

//Accelerometer Values
static int8_t x = 0;
static int8_t y = 0;
static int8_t z = 0;
static int32_t xoff = 0;
static int32_t yoff = 0;
static int32_t zoff = 0;

//Temperature Values
static int32_t temp1 = 0;
static int32_t temp2 = 0;

//Light Value
volatile static uint32_t lightVal = 0;
volatile static uint32_t lightINT = 0;

//Time
volatile static uint32_t timeMs = 0;
volatile static uint32_t timeLast = 0;
volatile static uint32_t timeBatt = 0;

//16-led mask
volatile static uint16_t ledOn = 0x0001;

//Switches
volatile static int modeSwitch = 2;
volatile static int activeMode = 5;
volatile static int catInit = 0;
volatile static int sw4 = 1;
volatile static int linkEstab = 0;
volatile static int SW3Press = 0;


// Main Functions
int main (void) {
	initializer();
	loop();
	return 1;
}

static void loop() {
	while (1)
	{

			if(SW3Press){
				SW3Press = 0;
				char tmp[20];
				int tickTEMP = timeMs;
				sprintf(tmp, "TB: %d.%d", (int) tickTEMP/1000, (int) tickTEMP%1000);
				oled_putString(0, 0, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);

				temp1 = temp_read();
				temp2 = temp_read();

				tickTEMP = timeMs;
				sprintf(tmp, "TA: %d.%d", (int) tickTEMP/1000, (int) tickTEMP%1000);
				oled_putString(0, 8, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);

				sprintf(tmp, "TempA: %d.%d", (int) temp1/10, (int) temp1%10);
				oled_putString(0, 40, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
				sprintf(tmp, "TempB: %d.%d", (int) temp2/10, (int) temp2%10);
				oled_putString(0, 48, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);

				Timer0_Wait(1000);
			}

	}
}


//Interrupt Handlers
void SysTick_Handler(void)  {
	timeMs++;
}

void EINT0_IRQHandler(void) {
	// SW3 as external interrupt through EINT0
	if ((LPC_SC->EXTINT>>0) & 0x1) {

			SW3Press = 1;

		LPC_SC -> EXTINT = (1 << 0); // clear interrupt status
	}
	return;
}

void EINT3_IRQHandler(void){

	if ((LPC_GPIOINT->IO2IntStatF>>5) & 0x1) { // light interrupt, right now it's not setting any flag{
		lightINT=9898;
		light_clearIrqStatus();
		LPC_GPIOINT->IO2IntClr = (1 << 5);
	}

	/*
	// joystick press
	if ((LPC_GPIOINT->IO0IntStatF>>17) & 0x1) {
		joystick_pressed = 1;
		LPC_GPIOINT->IO0IntClr = 1 << 17;
	}

	// joystick up
	if ((LPC_GPIOINT->IO2IntStatF>>3) & 0x1) {
		joystick_up = 1;
		LPC_GPIOINT->IO2IntClr = 1 << 3;
	}

	// joystick down (take note this mappning is not the same as the manual, )
	if ((LPC_GPIOINT->IO0IntStatF>>15) & 0x1) {
		joystick_down = 1;
		LPC_GPIOINT->IO0IntClr = 1 << 15;
	}

	// joystick left (take note this mappning is not the same as the manual)
	if ((LPC_GPIOINT->IO2IntStatF>>4) & 0x1) {
		joystick_left = 1;
		LPC_GPIOINT->IO2IntClr = 1 << 4;
	}

	//joystick right
	if ((LPC_GPIOINT->IO0IntStatF>>16) & 0x1) {
		joystick_right = 1;
		LPC_GPIOINT->IO0IntClr = 1 << 16;
	}
	*/

	return;
}

//Initializers
static void initializer(){
	init_i2c();
	init_ssp();
	init_SW3();

	oled_init();

	// SysTick Init
	SysTick_Config(SystemCoreClock / 1000);

	// Temp Init
	temp_init(&getTick);


	init_nvic();
	oled_clearScreen(OLED_COLOR_BLACK);
}

static void init_ssp(void)
{
	SSP_CFG_Type SSP_ConfigStruct;
	PINSEL_CFG_Type PinCfg;

	/*
	 * Initialize SPI pin connect
	 * P0.7 - SCK;
	 * P0.8 - MISO
	 * P0.9 - MOSI
	 * P2.2 - SSEL - used as GPIO
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 8;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 9;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Funcnum = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 2;
	PINSEL_ConfigPin(&PinCfg);

	SSP_ConfigStructInit(&SSP_ConfigStruct);

	// Initialize SSP peripheral with parameter given in structure above
	SSP_Init(LPC_SSP1, &SSP_ConfigStruct);

	// Enable SSP peripheral
	SSP_Cmd(LPC_SSP1, ENABLE);
}

static void init_i2c(void)
{
	PINSEL_CFG_Type PinCfg;

	/* Initialize I2C2 pin connect */
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 10;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 11;
	PINSEL_ConfigPin(&PinCfg);

	// Initialize I2C peripheral
	I2C_Init(LPC_I2C2, 100000);

	/* Enable I2C1 operation */
	I2C_Cmd(LPC_I2C2, ENABLE);
}

static void init_nvic(void)
{
	NVIC_SetPriority(SysTick_IRQn, 0x00);
	NVIC_SetPriority(EINT0_IRQn, 0x01);
	NVIC_SetPriority(EINT3_IRQn, 0x02);
}

static void init_light(void)
{


	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 5;
	PINSEL_ConfigPin(&PinCfg);

	light_clearIrqStatus();



	light_init();
	light_enable();
	light_setRange(LIGHT_RANGE_4000);
	//light_setHiThreshold(900);
	//light_setLoThreshold(50);
	light_setIrqInCycles(LIGHT_CYCLE_4);



	LPC_GPIOINT->IO2IntEnF |= (1<<5);
	NVIC_EnableIRQ(EINT3_IRQn);

}

static void init_SW3(void)
{
	/* setup SW3 as GPIO interrupt through EINT3
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 0; // GPIO
	PinCfg.OpenDrain = 0; // not enabled
	PinCfg.Pinmode = 0; // pull-up
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 10;
	PINSEL_ConfigPin(&PinCfg);

	LPC_GPIOINT->IO2IntClr = 1 << 10;	// clear interrupt status
	LPC_GPIOINT->IO2IntEnF |= 1 << 10;	// trigger at falling edge
	NVIC_EnableIRQ(EINT3_IRQn);			// enable through EINT3
	 */

	// setup SW3 as external interrupt through EINT0
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 1; // external interrupt EINT0
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 10;
	PINSEL_ConfigPin(&PinCfg);
	LPC_SC -> EXTINT |= 1 << 0;		// clear interrupt status
	LPC_SC -> EXTMODE |= 1 << 0;	// set external interrupt to be edge-sensitive (write 1)
	LPC_SC -> EXTPOLAR |= 0 << 0;	// trigger at falling edge

	NVIC_EnableIRQ(EINT0_IRQn);		// enable through EINT0
}

static void init_SW4(void)
{
	// Initialize button
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 1;
	PinCfg.Pinnum = 31;
	PINSEL_ConfigPin(&PinCfg);

	GPIO_SetDir(1, 1<<31, 0);
}

static void init_joystick(void) {

	PINSEL_CFG_Type PinCfg;

	// P0.17 - PIO2_0 (press)
	PinCfg.Funcnum = 0;		// use as GPIO
	PinCfg.OpenDrain = 0;	// not enabled
	PinCfg.Pinmode = 0;		// pull-up
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 17;
	PINSEL_ConfigPin(&PinCfg);

	// P2.3 - PIO2_3 (up)
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 3;
	PINSEL_ConfigPin(&PinCfg);

	// P0.15 - PIO2_1 (left)
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 15;
	PINSEL_ConfigPin(&PinCfg);

	// P2.4	- PIO2_4 (down)
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 4;
	PINSEL_ConfigPin(&PinCfg);

	// P0.16 - PIO2_2 (right)
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 16;
	PINSEL_ConfigPin(&PinCfg);

	LPC_GPIOINT->IO0IntClr = 1 << 17;
	LPC_GPIOINT->IO0IntEnF |= 1 << 17;

	LPC_GPIOINT->IO0IntClr = 1 << 15;
	LPC_GPIOINT->IO0IntEnF |= 1 << 15;

	LPC_GPIOINT->IO2IntClr = 1 << 3;
	LPC_GPIOINT->IO2IntEnF |= 1 << 3;

	LPC_GPIOINT->IO0IntClr = 1 << 16;
	LPC_GPIOINT->IO0IntEnF |= 1 << 16;

	LPC_GPIOINT->IO2IntClr = 1 << 4;
	LPC_GPIOINT->IO2IntEnF |= 1 << 4;

	NVIC_EnableIRQ(EINT3_IRQn);
}

static void init_UART(void){
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 2;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 0;
	PINSEL_ConfigPin(&PinCfg);

	PinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&PinCfg);

	UART_CFG_Type uartCfg;
	uartCfg.Baud_rate = 115200;
	uartCfg.Databits = UART_DATABIT_8;
	uartCfg.Parity = UART_PARITY_NONE;
	uartCfg.Stopbits = UART_STOPBIT_1;

	UART_Init(UART_DEV, &uartCfg);

	UART_TxCmd(UART_DEV, ENABLE);
}

//Etc
static uint32_t getTick(void)  {
	return timeMs;
}

static void modeToggle(){
	if(modeSwitch!=0){ //Switch to CAT Mode
		modeSwitch = 0;
		catInit = 1;
	}else{ //Switch to ACTIVE Mode
		modeSwitch = 1;
		activeMode = 5;
		linkEstab = 0;
		lightINT=9898;

		ledAllOff();
		led7seg_setChar(' ', FALSE);
		oled_clearScreen(OLED_COLOR_BLACK);
		rgb_setLeds (RGB_GREEN);

		timeBatt = getTick();
		timeLast = getTick();
	}

}

static void ledAllOff(){
	pca9532_setLeds(0x0000, 0xffff);
}

static void clearOLEDLower(){
	oled_putString(0, 8, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 16, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 24, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 32, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 40, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 48, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
}

void check_failed(uint8_t *file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while(1);
}

