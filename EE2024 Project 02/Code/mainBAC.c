/*****************************************************************************
 *   A demo example using several of the peripherals on the base board
 *
 *   Copyright(C) 2011, EE2024
 *   All rights reserved.
 *
 ******************************************************************************/

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_timer.h"
#include "lpc17xx_uart.h"

#include "joystick.h"
#include "pca9532.h"
#include "acc.h"
#include "oled.h"
#include "rgb.h"
#include "light.h"
#include "string.h"
#include "temp.h"
#include "led7seg.h"
#include "main.h"
//#include "uart2.h" //Not Needed
#include <stdio.h>

//Static Variables
//Joystick
static volatile int8_t joystick_down = 0;
static volatile int8_t joystick_up = 0;
static volatile int8_t joystick_left = 0;
static volatile int8_t joystick_right = 0;
static volatile int8_t joystick_pressed = 0;

//Accelerometer Values
static int8_t x = 0;
static int8_t y = 0;
static int8_t z = 0;
static int32_t xoff = 0;
static int32_t yoff = 0;
static int32_t zoff = 0;

//Temperature Values
static int32_t temp1 = 0;
static int32_t temp2 = 0;

//Light Value
volatile static uint32_t lightVal = 0;
volatile static uint32_t lightINT = 0;

//Time
volatile static uint32_t timeMs = 0;
volatile static uint32_t timeLast = 0;
volatile static uint32_t timeBatt = 0;

//16-led mask
volatile static uint16_t ledOn = 0x0001;

//Switches
volatile static int modeSwitch = 2;
volatile static int activeMode = 5;
volatile static int catInit = 0;
volatile static int sw4 = 1;
volatile static int linkEstab = 0;
volatile static int SW3Press = 0;


// Main Functions
int main (void) {
	initializer();
	loop();
	return 1;
}

static void loop() {
	while (1)
	{
		/*
		// START TESTING INTERRUPT
		if (SW3Press == 1) {
			printf("sw3 \r\n");
			SW3Press = 0;
		}
		if (joystick_pressed == 1) {
			printf("press \r\n");
			joystick_pressed = 0;
		}
		if (joystick_down == 1) {
			printf("down \r\n");
			joystick_down = 0;
		}
		if (joystick_up == 1) {
			printf("up \r\n");
			joystick_up = 0;
		}
		if (joystick_left == 1) {
			printf("left \r\n");
			joystick_left = 0;
		}
		if (joystick_right == 1) {
			printf("right \r\n");
			joystick_right = 0;
		}
		// END TESTING INTERRUPT

		//		if (flag_switch_mode == 1) // active
		//		{
		//			modeToggle();
		//			flag_switch_mode = -1;
		//		}

		 */


		sw4_read();

		if(modeSwitch == 1){ //ACTIVE MODE

			if(SW3Press){
				readSensors();
				print();
				sendData();
				SW3Press=0;
			}

			if(lightINT==9898){
				lightINT = light_read();
				if(lightINT>50&&lightINT<900){
					light_setHiThreshold(900);
					light_setLoThreshold(50);
				}else if(lightINT<50){
					light_setHiThreshold(50);
					light_setLoThreshold(0);
				}else if(lightINT>900){
					light_setHiThreshold(4000);
					light_setLoThreshold(900);
				}
			}

			if(lightINT>900){//Full Performance

				if(activeMode!=0){
					activeMode=0;
					readSensors();
					oled_clearScreen(OLED_COLOR_BLACK);
					oled_putString(0, 0, (uint8_t*) "ACTIVE - FP", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
					print();
				}

				if(getTick()-timeLast >= SENSOR_SAMPLING_TIME){
					readSensors();
					print();

					if(linkEstab==1&&pca9532_getLedState(0)==0xffff){
						sendData();
					}

					timeLast = getTick();
				}

				if(getTick()-timeBatt >= INDICATOR_TIME_UNIT){
					if(pca9532_getLedState(0)==0x0000){
						pca9532_setLeds(0x0001, 0xffff);
					}else{
						ledOn = (pca9532_getLedState(0))|(pca9532_getLedState(0)<<1);
						pca9532_setLeds(ledOn, 0xffff);
						if(linkEstab==0&&pca9532_getLedState(0)==0xffff){
							UART_SendString(UART_DEV, (uint8_t*)"Satellite Communication Link Established\r\n");
							linkEstab = 1;
						}
					}
					timeBatt = getTick();
				}
			}else if (lightINT<50){//Power Saver

				if(activeMode!=1){
					activeMode=1;
					print_PS();
				}

				if(getTick()-timeLast >= SENSOR_SAMPLING_TIME){
					timeLast = getTick();
				}

				if(getTick()-timeBatt >= INDICATOR_TIME_UNIT){
					ledOn = (pca9532_getLedState(0))&(pca9532_getLedState(0)>>1);
					pca9532_setLeds(ledOn, 0xffff);
					if(pca9532_getLedState(0)==(0xffff>>1)){
						UART_SendString(UART_DEV, (uint8_t*)"Satellite Communication Link Suspended\r\n");
						linkEstab = 0;
					}

					timeBatt = getTick();
				}

			}else{//Normal Operation
				if(activeMode!=2){
					activeMode=2;
					readSensors();
					oled_clearScreen(OLED_COLOR_BLACK);
					oled_putString(0, 0, (uint8_t*) "ACTIVE - NO", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
					print();
				}
				if(getTick()-timeLast >= SENSOR_SAMPLING_TIME){
					readSensors();
					timeLast = getTick();
				}

				if(getTick()-timeBatt >= INDICATOR_TIME_UNIT){
					timeBatt = getTick();
				}
			}
		}else if(modeSwitch == 0){ //CAT MODE
			if(catInit){
				initCAT();
			}
			// Sensor Update, every 4 seconds
			if(!modeSwitch&&getTick()-timeLast >= SENSOR_SAMPLING_TIME){
				readSensors();

				oled_clearScreen(OLED_COLOR_BLACK);
				oled_putString(0, 0, (uint8_t*) "Sensor Status", OLED_COLOR_BLACK, OLED_COLOR_WHITE);

				print();
				timeLast = getTick();
			}
		}


	}
}


//Input
void readSensors(){
	acc_read(&x, &y, &z);
	x = x+xoff;
	y = y+yoff;
	z = z+zoff;

	lightVal = light_read();
	temp1 = temp_read();
	temp2 = temp_read();
}

void sw4_read(){
	sw4 = (GPIO_ReadValue(1) >> 31) & 0x01;

	if(sw4==0){
		Timer0_Wait(500);
		modeToggle();
	}
}

//Output
void print(){
	clearOLEDLower();
	char tmp[20];

	// ACCEL START
	sprintf(tmp, "AX: %d", x);
	oled_putString(0, 8, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	sprintf(tmp, "AY: %d", y);
	oled_putString(0, 16, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	sprintf(tmp, "AZ: %d", z);
	oled_putString(0, 24, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);

	//LIGHT START
	sprintf(tmp, "L:  %u", (unsigned int) lightVal);
	oled_putString(0, 32, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);

	//TEMP START
	sprintf(tmp, "TA: %d.%d", (int) temp1/10, (int) temp1%10);
	oled_putString(0, 40, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	sprintf(tmp, "TB: %d.%d", (int) temp2/10, (int) temp2%10);
	oled_putString(0, 48, (uint8_t*) tmp, OLED_COLOR_WHITE, OLED_COLOR_BLACK);

}

void print_PS(){
	oled_clearScreen(OLED_COLOR_BLACK);
	oled_putString(0, 0, (uint8_t*) "ACTIVE - PS", OLED_COLOR_BLACK, OLED_COLOR_WHITE);

	// ACCEL
	oled_putString(0, 8, (uint8_t*) "AX: PS", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 16, (uint8_t*) "AY: PS", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 24, (uint8_t*) "AZ: PS", OLED_COLOR_WHITE, OLED_COLOR_BLACK);

	//LIGHT
	oled_putString(0, 32, (uint8_t*) "L:  PS", OLED_COLOR_WHITE, OLED_COLOR_BLACK);

	//TEMP
	oled_putString(0, 40, (uint8_t*) "TA: PS", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 48, (uint8_t*) "TB: PS", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
}

void sendData(){
	char scan[50];

	sprintf(scan, "L%d_TA%ld.%ld_TB%ld.%ld_AX%d_AY%d_AZ%d\r\n", (unsigned int) lightVal, temp1/10, temp1%10, temp2/10, temp2%10, x, y, z);

	UART_SendString(UART_DEV, (uint8_t*) scan);
}

//CAT_MODE_HELPERS
static void blink_Blue(){
	rgb_setLeds (0x00);

	uint32_t blinkTick1 = getTick();
	uint32_t blinkTick2 = getTick();

	int tog = 0;
	while(getTick()-blinkTick1 <= SENSOR_SAMPLING_TIME){

		if(tog == 0 && getTick()-blinkTick2 >= 500){
			rgb_setLeds (RGB_BLUE);
			tog = 1;
			blinkTick2 = getTick();
		}else if(tog == 1 && getTick()-blinkTick2 >= 500) {
			rgb_setLeds (RGB_GREEN); // Actually shuts off LED
			tog = 0;
			blinkTick2 = getTick();
		}
		sw4_read();
		if(modeSwitch==1){
			return;
		}
	}
}

static void blink_Red(){
	rgb_setLeds (0x00);

	uint32_t blinkTick1 = getTick();
	uint32_t blinkTick2 = getTick();

	int tog = 0;
	while(getTick()-blinkTick1 <= SENSOR_SAMPLING_TIME){

		if(tog == 0 && getTick()-blinkTick2 >= 500){
			rgb_setLeds (RGB_RED);
			tog = 1;
			blinkTick2 = getTick();
		}else if(tog == 1 && getTick()-blinkTick2 >= 500) {
			rgb_setLeds (RGB_GREEN); // Actually shuts off LED
			tog = 0;
			blinkTick2 = getTick();
		}
		sw4_read();
		if(modeSwitch==1){
			return;
		}
	}
}

static void seven_count(){
	int i = 1;
	uint32_t sevenTick = getTick();

	led7seg_setChar('0', TRUE);
	while(i != 6){
		if(getTick()-sevenTick >= 1000){
			if(i==1){
				led7seg_setChar('1', TRUE);
			}else if(i==2){
				led7seg_setChar('2', TRUE);
			}else if(i==3){
				led7seg_setChar('3', TRUE);
			}else if(i==4){
				led7seg_setChar('4', TRUE);
			}else if(i==5){
				led7seg_setChar('5', TRUE);
			}
			sevenTick = getTick();
			i++;
		}
		sw4_read();
		if(modeSwitch==1) return;
	}
}

static void battery_count(){
	int i = 0;
	uint32_t batteryTick = getTick();

	while(i < 16){
		if(getTick()-batteryTick >= INDICATOR_TIME_UNIT){
			if(i==0){
				pca9532_setLeds(0x0001, 0xffff);
			}else{
				ledOn = (pca9532_getLedState(0))|(pca9532_getLedState(0)<<1);
				pca9532_setLeds(ledOn, 0xffff);
			}
			batteryTick = getTick();
			i++;
		}
		sw4_read();
		if(modeSwitch==1) return;
	}
}

//Interrupt Handlers
void SysTick_Handler(void)  {
	timeMs++;
}

void EINT0_IRQHandler(void) {
	// SW3 as external interrupt through EINT0
	if ((LPC_SC->EXTINT>>0) & 0x1) {
		if(modeSwitch==1){
			SW3Press = 1;
		}
		LPC_SC -> EXTINT = (1 << 0); // clear interrupt status
	}
	return;
}

void EINT3_IRQHandler(void){

	if ((LPC_GPIOINT->IO2IntStatF>>5) & 0x1) { // light interrupt, right now it's not setting any flag{
		lightINT=9898;
		light_clearIrqStatus();
		LPC_GPIOINT->IO2IntClr = (1 << 5);
	}

	/*
	// joystick press
	if ((LPC_GPIOINT->IO0IntStatF>>17) & 0x1) {
		joystick_pressed = 1;
		LPC_GPIOINT->IO0IntClr = 1 << 17;
	}

	// joystick up
	if ((LPC_GPIOINT->IO2IntStatF>>3) & 0x1) {
		joystick_up = 1;
		LPC_GPIOINT->IO2IntClr = 1 << 3;
	}

	// joystick down (take note this mappning is not the same as the manual, )
	if ((LPC_GPIOINT->IO0IntStatF>>15) & 0x1) {
		joystick_down = 1;
		LPC_GPIOINT->IO0IntClr = 1 << 15;
	}

	// joystick left (take note this mappning is not the same as the manual)
	if ((LPC_GPIOINT->IO2IntStatF>>4) & 0x1) {
		joystick_left = 1;
		LPC_GPIOINT->IO2IntClr = 1 << 4;
	}

	//joystick right
	if ((LPC_GPIOINT->IO0IntStatF>>16) & 0x1) {
		joystick_right = 1;
		LPC_GPIOINT->IO0IntClr = 1 << 16;
	}
	*/

	return;
}

//Initializers
static void initializer(){
	init_i2c();
	init_ssp();
	init_SW4();
	init_SW3();

	init_UART();

	// Not Needed
	//uart2_init(115200, CHANNEL_A);

	pca9532_init();
	oled_init();
	led7seg_init();
	rgb_init();
	acc_init();
	init_joystick();
	//	light_init();
	//	light_enable();
	init_light();
	init_nvic();




	// SysTick Init
	SysTick_Config(SystemCoreClock / 1000);

	// Temp Init
	temp_init(&getTick);


	oled_clearScreen(OLED_COLOR_BLACK);
	ledAllOff();
	led7seg_setChar(' ', FALSE);
}

void initCAT(){
	oled_clearScreen(OLED_COLOR_BLACK);
	ledAllOff();

	sw4_read();
	if(modeSwitch==1) return;

	oled_putString(0, 0, (uint8_t*) "I-WATCH", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
	oled_putString(0, 8, (uint8_t*) "Electronic Tag", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
	oled_putString(0, 16, (uint8_t*) "Configuration", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
	oled_putString(0, 24, (uint8_t*) "and Testing", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
	oled_putString(0, 32, (uint8_t*) "Mode", OLED_COLOR_BLACK, OLED_COLOR_WHITE);

	sw4_read();


	blink_Blue();
	sw4_read();
	if(modeSwitch==1) return;

	blink_Red();
	sw4_read();
	if(modeSwitch==1) return;

	rgb_setLeds (RGB_RED);
	sw4_read();
	if(modeSwitch==1) return;

	seven_count();
	sw4_read();
	if(modeSwitch==1) return;

	battery_count();
	sw4_read();
	if(modeSwitch==1) return;

	acc_read(&x, &y, &z);
	xoff = 0-x;
	yoff = 0-y;
	zoff = 64-z;
	catInit = 0;
}

static void init_ssp(void)
{
	SSP_CFG_Type SSP_ConfigStruct;
	PINSEL_CFG_Type PinCfg;

	/*
	 * Initialize SPI pin connect
	 * P0.7 - SCK;
	 * P0.8 - MISO
	 * P0.9 - MOSI
	 * P2.2 - SSEL - used as GPIO
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 8;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 9;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Funcnum = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 2;
	PINSEL_ConfigPin(&PinCfg);

	SSP_ConfigStructInit(&SSP_ConfigStruct);

	// Initialize SSP peripheral with parameter given in structure above
	SSP_Init(LPC_SSP1, &SSP_ConfigStruct);

	// Enable SSP peripheral
	SSP_Cmd(LPC_SSP1, ENABLE);
}

static void init_i2c(void)
{
	PINSEL_CFG_Type PinCfg;

	/* Initialize I2C2 pin connect */
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 10;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 11;
	PINSEL_ConfigPin(&PinCfg);

	// Initialize I2C peripheral
	I2C_Init(LPC_I2C2, 100000);

	/* Enable I2C1 operation */
	I2C_Cmd(LPC_I2C2, ENABLE);
}

static void init_nvic(void)
{
	NVIC_SetPriority(SysTick_IRQn, 0x00);
	NVIC_SetPriority(EINT0_IRQn, 0x01);
	NVIC_SetPriority(EINT3_IRQn, 0x02);
}

static void init_light(void)
{


	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 5;
	PINSEL_ConfigPin(&PinCfg);

	light_clearIrqStatus();



	light_init();
	light_enable();
	light_setRange(LIGHT_RANGE_4000);
	//light_setHiThreshold(900);
	//light_setLoThreshold(50);
	light_setIrqInCycles(LIGHT_CYCLE_4);



	LPC_GPIOINT->IO2IntEnF |= (1<<5);
	NVIC_EnableIRQ(EINT3_IRQn);

}

static void init_SW3(void)
{
	/* setup SW3 as GPIO interrupt through EINT3
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 0; // GPIO
	PinCfg.OpenDrain = 0; // not enabled
	PinCfg.Pinmode = 0; // pull-up
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 10;
	PINSEL_ConfigPin(&PinCfg);

	LPC_GPIOINT->IO2IntClr = 1 << 10;	// clear interrupt status
	LPC_GPIOINT->IO2IntEnF |= 1 << 10;	// trigger at falling edge
	NVIC_EnableIRQ(EINT3_IRQn);			// enable through EINT3
	 */

	// setup SW3 as external interrupt through EINT0
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 1; // external interrupt EINT0
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 10;
	PINSEL_ConfigPin(&PinCfg);
	LPC_SC -> EXTINT |= 1 << 0;		// clear interrupt status
	LPC_SC -> EXTMODE |= 1 << 0;	// set external interrupt to be edge-sensitive (write 1)
	LPC_SC -> EXTPOLAR |= 0 << 0;	// trigger at falling edge

	NVIC_EnableIRQ(EINT0_IRQn);		// enable through EINT0
}

static void init_SW4(void)
{
	// Initialize button
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 1;
	PinCfg.Pinnum = 31;
	PINSEL_ConfigPin(&PinCfg);

	GPIO_SetDir(1, 1<<31, 0);
}

static void init_joystick(void) {

	PINSEL_CFG_Type PinCfg;

	// P0.17 - PIO2_0 (press)
	PinCfg.Funcnum = 0;		// use as GPIO
	PinCfg.OpenDrain = 0;	// not enabled
	PinCfg.Pinmode = 0;		// pull-up
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 17;
	PINSEL_ConfigPin(&PinCfg);

	// P2.3 - PIO2_3 (up)
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 3;
	PINSEL_ConfigPin(&PinCfg);

	// P0.15 - PIO2_1 (left)
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 15;
	PINSEL_ConfigPin(&PinCfg);

	// P2.4	- PIO2_4 (down)
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 4;
	PINSEL_ConfigPin(&PinCfg);

	// P0.16 - PIO2_2 (right)
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 16;
	PINSEL_ConfigPin(&PinCfg);

	LPC_GPIOINT->IO0IntClr = 1 << 17;
	LPC_GPIOINT->IO0IntEnF |= 1 << 17;

	LPC_GPIOINT->IO0IntClr = 1 << 15;
	LPC_GPIOINT->IO0IntEnF |= 1 << 15;

	LPC_GPIOINT->IO2IntClr = 1 << 3;
	LPC_GPIOINT->IO2IntEnF |= 1 << 3;

	LPC_GPIOINT->IO0IntClr = 1 << 16;
	LPC_GPIOINT->IO0IntEnF |= 1 << 16;

	LPC_GPIOINT->IO2IntClr = 1 << 4;
	LPC_GPIOINT->IO2IntEnF |= 1 << 4;

	NVIC_EnableIRQ(EINT3_IRQn);
}

static void init_UART(void){
	PINSEL_CFG_Type PinCfg;

	PinCfg.Funcnum = 2;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 0;
	PINSEL_ConfigPin(&PinCfg);

	PinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&PinCfg);

	UART_CFG_Type uartCfg;
	uartCfg.Baud_rate = 115200;
	uartCfg.Databits = UART_DATABIT_8;
	uartCfg.Parity = UART_PARITY_NONE;
	uartCfg.Stopbits = UART_STOPBIT_1;

	UART_Init(UART_DEV, &uartCfg);

	UART_TxCmd(UART_DEV, ENABLE);
}

//Etc
static uint32_t getTick(void)  {
	return timeMs;
}

static void modeToggle(){
	if(modeSwitch!=0){ //Switch to CAT Mode
		modeSwitch = 0;
		catInit = 1;
	}else{ //Switch to ACTIVE Mode
		modeSwitch = 1;
		activeMode = 5;
		linkEstab = 0;
		lightINT=9898;

		ledAllOff();
		led7seg_setChar(' ', FALSE);
		oled_clearScreen(OLED_COLOR_BLACK);
		rgb_setLeds (RGB_GREEN);

		timeBatt = getTick();
		timeLast = getTick();
	}

}

static void ledAllOff(){
	pca9532_setLeds(0x0000, 0xffff);
}

static void clearOLEDLower(){
	oled_putString(0, 8, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 16, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 24, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 32, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 40, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
	oled_putString(0, 48, (uint8_t*) "                          ", OLED_COLOR_WHITE, OLED_COLOR_BLACK);
}

void check_failed(uint8_t *file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while(1);
}

