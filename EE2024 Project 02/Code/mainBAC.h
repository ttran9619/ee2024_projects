/*
 * main.h
 *
 *  Created on: Apr 3, 2016
 *      Author: Tyler Tran
 */

#ifndef MAIN_H_
#define MAIN_H_

#define MODE_CAT 1
#define MODE_ACTIVE 0

#define INDICATOR_TIME_UNIT 250
#define SENSOR_SAMPLING_TIME 4000

#define UART_DEV LPC_UART3 //CHECK



//Prototypes
static void loop();

//Input
void readSensors();
void sw4_read();

//Output
void print();
void print_PS();
void sendData();

//CAT_MODE_HELPERS
static void blink_Blue();
static void blink_Red();
static void seven_count();
static void battery_count();

//Interrupt Handlers
void SysTick_Handler(void);
void EINT3_IRQHandler(void);
void EINT0_IRQHandler(void);

//Initializers
static void initializer();
void initCAT();
static void init_ssp(void);
static void init_i2c(void);
static void init_nvic(void);
static void init_light(void);
static void init_SW3(void);
static void init_SW4(void);
static void init_joystick(void);
static void init_UART(void);

//Etc
static uint32_t getTick(void);
static void modeToggle();
static void ledAllOff();
static void clearOLEDLower();
void check_failed(uint8_t *file, uint32_t line);


#endif
